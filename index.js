function Usuario(nombre, apellido, email, pais, contrasena){
    this.nombre = nombre;
    this.apellido = apellido;
    this.email = email;
    this.pais = pais;
    this.contrasena = contrasena;
}

function validarRegistro(e) {

    e.preventDefault();
    const nombre = document.querySelector('#nombre').value;
    const apellido = document.querySelector('#apellido').value;
    const email = document.querySelector('#email').value;
    const pais = document.querySelector('#pais').value;
    const contrasena = document.querySelector('#pass1').value;
    const contrasena2 = document.querySelector('#pass2').value;

    if (nombre === "" || apellido === "" || email === "" || pais === "" || contrasena === "") {
            alert("Complete todos los campos");
    } else if (contrasena !== contrasena2) {
            alert("Las contraseñas no son iguales");
    } else {
            if (listaUsuarios.length === 0) {
                    const usuario = new Usuario(nombre, apellido, email, pais, contrasena);
                    listaUsuarios.push(usuario);
                    console.log(listaUsuarios);
            } else {
                for (usuario of listaUsuarios){
                    if (usuario.email === email) {
                        alert("Usuario ya existente");
                    } else {
                        const usuario = new Usuario(nombre, apellido, email, pais, contrasena);
                        listaUsuarios.push(usuario);
                        console.log(listaUsuarios);
                    }
                }
            }
        }
}

function loginUsuario(e) {
    e.preventDefault();
    const emailLogin = document.querySelector('#emailLogin').value;
    const passLogin = document.querySelector('#passLogin').value;

    for (let i=0; i<listaUsuarios.length; i++) {
        if (listaUsuarios[i].email === emailLogin) {
            if (listaUsuarios[i].contrasena === passLogin){
                console.log(i);
                return true;
            } else {
                alert('Contraseña inválida');
            }
        } else {
            alert('Usuario no encontrado');
            return false;
        }
    }
}

listaUsuarios = [];

function main(){
    document.querySelector('#registrar').addEventListener('click', validarRegistro);
    document.querySelector('#loginUsuario').addEventListener('click', loginUsuario);
}

main();